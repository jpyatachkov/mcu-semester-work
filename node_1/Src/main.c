/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body of mcu which is connected with PC via
                         USB cable
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
CRC_HandleTypeDef hcrc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart1;  // Used for Wi-Fi module

UART_HandleTypeDef huart3;
UART_HandleTypeDef huart4;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define BUFFER_SIZE USB_PACKET_SIZE + 4  // + 4 for CRC

uint8_t        USB_Data[USB_PACKET_SIZE] = {0}; // Data from USB (used in usb_cdc_if.c)
uint8_t        Serial_Data[BUFFER_SIZE]  = {0}; // Data to transmit between MCUs

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void        SystemClock_Config(void);

void        Error_Handler(void);

static void MX_GPIO_Init(void);

static void MX_CRC_Init(void);

static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);

static void MX_USART1_UART_Init(void);

static void MX_USART3_UART_Init(void);
static void MX_USART4_UART_Init(void);

static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* LED API -------------------------------------------------------------------*/

const uint8_t N_LEDS = 4;

const unsigned long ledMasks[] = {GPIO_PIN_6, GPIO_PIN_7, 
                                  GPIO_PIN_8, GPIO_PIN_9,
                                  GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9};

typedef enum {
  RED,
  BLUE, 
  ORANGE, 
  GREEN,
  ALL
} LED_Color;

static void LED_Init(void);

void LED_TurnOn(uint8_t led);
void LED_TurnOff(uint8_t led);

void LED_Blink(uint8_t led);

/* Wi-Fi module API ----------------------------------------------------------*/

static void ESP_Init(void);

static void ESP_SendCommand(const char *);

/* CRC module API ------------------------------------------------------------*/

static void     CRC_Calculate(uint8_t *pData, uint8_t size);

static uint8_t  CRC_Check(uint8_t *pData, uint8_t size);

static uint32_t CRC_GetFromPacket(uint8_t *pData);

/* UART module API -----------------------------------------------------------*/

static uint8_t UART_ReceptionComplete    = RESET;  // Flag of complete reception via UART
static uint8_t UART_TransmissionComplete = RESET;  // Flag of complete transmission via UART

uint8_t UART_CheckReceptionComplete(void);
uint8_t UART_CheckTransmissionComplete(void);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

/* SPI module API ------------------------------------------------------------*/

static uint8_t SPI_ReceptionComplete    = RESET; // Flag of complete reception via SPI
static uint8_t SPI_TransmissionComplete = RESET; // Flag of complete transmission via SPI

uint8_t SPI_CheckReceptionComplete(void);
uint8_t SPI_CheckTransmissionComplete(void);

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *huart);
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *huart);

/* Helper functions ----------------------------------------------------------*/

static uint8_t _get_size(uint8_t *pData);
static void    _itoa(uint8_t num, uint8_t *str);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{
  
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  
  MX_CRC_Init();
  
  MX_SPI1_Init();
  MX_SPI2_Init();
  
  MX_USART1_UART_Init();
  
  MX_USART3_UART_Init();
  MX_USART4_UART_Init();
  
  /* Configure user USB */
  MX_USB_DEVICE_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();

  /* USER CODE BEGIN 2 */
  
  LED_Init();
  
  ESP_Init();
  
  HAL_Delay(100);
  
  LED_TurnOn(GREEN);
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
    if (CDC_CheckPacketReceived())
    {
      LED_Blink(BLUE);
      
      memcpy(Serial_Data, USB_Data, USB_PACKET_SIZE);
      memset(USB_Data, 0, USB_PACKET_SIZE);
      
      CRC_Calculate(Serial_Data, strlen((const char *)Serial_Data)); 
      
      HAL_UART_Transmit_IT(&huart3, Serial_Data, BUFFER_SIZE);
      
      while (!UART_CheckTransmissionComplete());
      
      HAL_SPI_Receive_IT(&hspi2, Serial_Data, BUFFER_SIZE);
      
      LED_Blink(BLUE);
    }
    
    if (SPI_CheckReceptionComplete())
    {
      if (CRC_Check(Serial_Data, strlen((const char *)Serial_Data)))
      {
        LED_Blink(ORANGE);
        
        uint8_t size = _get_size(Serial_Data);
        
        const char *command = "AT+CIPSEND=0,";
      
        strcpy((char *)Serial_Data + size, "\r\n");
        
        char data_len[4] = {0};
        _itoa(size, (uint8_t *)data_len);
      
        char command_buffer[20] = {0};

        strcpy(command_buffer, command);
        strcpy(command_buffer + strlen(command_buffer), data_len);
        strcpy(command_buffer + strlen(command_buffer), "\r\n");
      
        ESP_SendCommand(command_buffer);
        ESP_SendCommand((const char *)Serial_Data);
      }
      else
      {
        LED_Blink(RED);
      }
      
      LED_Blink(BLUE);
    }
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State     = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState   = RCC_PLL_NONE;
  
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType      = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB | RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.UsbClockSelection    = RCC_USBCLKSOURCE_HSI48;
  
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
  
}

/** NVIC Configuration
*/
static void MX_NVIC_Init(void)
{
  
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
  
  /* SPI2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(SPI2_IRQn);
  
  /* SPI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(SPI1_IRQn);
  
  /* USART3_4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART3_4_IRQn);
  
  /* USB_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USB_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USB_IRQn);
  
}

/* CRC init function */
static void MX_CRC_Init(void)
{

  hcrc.Instance = CRC;
  
  hcrc.Init.DefaultPolynomialUse    = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse     = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode  = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat              = CRC_INPUTDATA_FORMAT_BYTES;
  
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  hspi1.Instance = SPI1;
  
  hspi1.Init.Mode           = SPI_MODE_SLAVE;
  hspi1.Init.Direction      = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize       = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity    = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase       = SPI_PHASE_1EDGE;
  hspi1.Init.NSS            = SPI_NSS_SOFT;
  hspi1.Init.FirstBit       = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode         = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.NSSPMode       = SPI_NSS_PULSE_DISABLE;
  
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  hspi2.Instance = SPI2;
  
  hspi2.Init.Mode              = SPI_MODE_MASTER;
  hspi2.Init.Direction         = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize          = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity       = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase          = SPI_PHASE_1EDGE;
  hspi2.Init.NSS               = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi2.Init.FirstBit          = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode            = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.NSSPMode          = SPI_NSS_PULSE_DISABLE;
  
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  
  huart1.Init.BaudRate               = 115200;
  huart1.Init.WordLength             = UART_WORDLENGTH_8B;
  huart1.Init.StopBits               = UART_STOPBITS_1;
  huart1.Init.Parity                 = UART_PARITY_NONE;
  huart1.Init.Mode                   = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling           = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  
  huart3.Init.BaudRate               = 9600;
  huart3.Init.WordLength             = UART_WORDLENGTH_8B;
  huart3.Init.StopBits               = UART_STOPBITS_1;
  huart3.Init.Parity                 = UART_PARITY_NONE;
  huart3.Init.Mode                   = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling           = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART4 init function */
static void MX_USART4_UART_Init(void)
{

  huart4.Instance = USART4;
  
  huart4.Init.BaudRate = 9600;
  
  huart4.Init.WordLength             = UART_WORDLENGTH_8B;
  huart4.Init.StopBits               = UART_STOPBITS_1;
  huart4.Init.Parity                 = UART_PARITY_NONE;
  huart4.Init.Mode                   = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling           = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Pinout Configuration
*/
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  
}

/* USER CODE BEGIN 4 */

/* LED API -------------------------------------------------------------------*/

/**
  * @brief  Initializes pins 6-9 of port C as outputs for board LEDs
  * @param  None
  * @retval None
  */
static void LED_Init(void)
{
  
  GPIO_InitTypeDef LED_Init;
  
  LED_Init.Mode  = GPIO_MODE_OUTPUT_PP;
  LED_Init.Pull  = GPIO_NOPULL;
  LED_Init.Pin   = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9;
  LED_Init.Speed = GPIO_SPEED_FREQ_LOW;

  HAL_GPIO_Init(GPIOC, &LED_Init);
  
}

/**
  * @brief  Turns on selected LED
  * @param  LED color (taken from LED_Color enum)
  * @retval None
  */
void LED_TurnOn(uint8_t led)
{
  
  if (led > N_LEDS)
  {
    return;
  }
  
  HAL_GPIO_WritePin(GPIOC, ledMasks[led], GPIO_PIN_SET);
  
}

/**
  * @brief  Turns off selected LED
  * @param  LED color (taken from LED_Color enum)
  * @retval None
  */
void LED_TurnOff(uint8_t led)
{
  
  if (led > N_LEDS)
  {
    return;
  }
  
  HAL_GPIO_WritePin(GPIOC, ledMasks[led], GPIO_PIN_RESET);
  
}

/**
  * @brief  Blinks selected LED
  * @param  LED color (taken from LED_Color enum)
  * @retval None
  */
void LED_Blink(uint8_t led)
{
  
  if (led > N_LEDS)
  {
    return;
  }
  
  HAL_GPIO_WritePin(GPIOC, ledMasks[led], GPIO_PIN_SET);
  HAL_Delay(50);
  HAL_GPIO_WritePin(GPIOC, ledMasks[led], GPIO_PIN_RESET);
  HAL_Delay(50);
  
}

/* Wi-Fi module API ----------------------------------------------------------*/

/**
  * @brief  Prepares ESP8266 Wi-Fi module for work in TCP server mode
  * @param  None
  * @retval None
  */
static void ESP_Init(void)
{
  
  // Reset module
  ESP_SendCommand("AT+RST\r\n");
  
  // UART baud rate settings
  ESP_SendCommand("AT+CIOBAUD=115200 BAUD->115200\r\n");
  HAL_Delay(200);
  
  // Configuring ESP8266 as TCP server and wi-fi hotspot
  ESP_SendCommand("AT+CWMODE=3\r\n");
  
  // Configuring server
  ESP_SendCommand("AT+CIPMODE=0\r\n");         // Receiving|transmitting server enabled
  ESP_SendCommand("AT+CIPMUX=1\r\n");          // Multiple connections enabled
  ESP_SendCommand("AT+CIPSERVER=1,8888\r\n");  // Start server on port 8888
  
}

/**
  * @brief  Sends command to ESP8266 via uart1 (uses name huart1 for it)
  * @param  Command (
  *         @note command must be terminated by \r\n
  * @retval None
  */
static void ESP_SendCommand(const char *command)
{
  
  if (command == NULL)
  {
    return;
  }
  
  HAL_UART_Transmit_IT(&huart1, (uint8_t *)command, strlen(command));
  HAL_Delay(500);
  
}

/* CRC module API ------------------------------------------------------------*/

/**
  * @brief  Calculates CRC for given array of data
  * @param  pData is a pointer to data array
  *         @note to make program work correct, use Serial_Data as parameter
  * @param  size is size of nonzero part of pData
  * @retval None
  *         @note This routine adds to the last 4 bytes of pData [USB_PACKET_SIZE .. BUFFER_SIZE]
                  calculated CRC to transmit data and its CRC via UART or SPI as entire packet
  */
static void CRC_Calculate(uint8_t *pData, uint8_t size)
{
  
  if (pData == NULL || !size)
  {
    return;
  }
  
  if (size >= USB_PACKET_SIZE)
  {
    size = USB_PACKET_SIZE;
  }
  
  size = (size % sizeof(uint32_t)) ? size / sizeof(uint32_t) + 1 : size / sizeof(uint32_t);
  
  uint32_t crc = HAL_CRC_Calculate(&hcrc, (uint32_t *)pData, size);
  
  // Adding CRC to last for bytes of array (
  for (int i = USB_PACKET_SIZE; i < BUFFER_SIZE; i++)
  {
    pData[i] = (uint8_t)(crc >> abs((USB_PACKET_SIZE - i) * 8));
  }
  
}

/**
  * @brief  Calculates packet CRC (entire array - 4 bytes) and compares obtained CRC
  *         with number which is held by last for bytes if array
  * @param  pData is a pointer to data array
  *         @note to make program work correct, use Serial_Data as parameter
  * @param  size is size of nonzero part of pData
  * @retval 0 if CRCs are not equal, 1 else
  */
static uint8_t  CRC_Check(uint8_t *pData, uint8_t size)
{
  
  if (pData == NULL || !size)
  {
    return 0;
  }
  
  size = (size % sizeof(uint32_t)) ? size / sizeof(uint32_t) + 1 : size / sizeof(uint32_t);
  
  uint32_t crc = HAL_CRC_Calculate(&hcrc, (uint32_t *)pData, size);
  
  return (crc == CRC_GetFromPacket(pData));
  
}

/**
  * @brief  Makes uint32_t from the last 4 bytes of uint8_t array
  * @param  pData is a pointer to data array
  * @retval uint32_t - packet CRC as entire number
  */
static uint32_t CRC_GetFromPacket(uint8_t *pData)
{
  
  if (pData == NULL)
  {
    return 0;
  }
  
  uint32_t crc = 0;
  
  for (uint8_t i = USB_PACKET_SIZE; i < BUFFER_SIZE; i++)
  {
    crc |= (uint32_t)(pData[i] << abs((USB_PACKET_SIZE - i) * 8));
  }
  
  return crc;
  
}

/* UART module API -----------------------------------------------------------*/

/**
  * @brief  Checks whether all data received via UART
  *         @note if all data received, it resets reception complete flag
  * @param  None
  * @retval 0 if no, 1 else
  */
uint8_t UART_CheckReceptionComplete(void)
{
  
  if (UART_ReceptionComplete)
  {
    UART_ReceptionComplete = RESET;
    return 1;
  }
  
  return 0;
  
}

/**
  * @brief  Checks whether all data transmitted via UART
  *         @note if all data transmitted, it resets transmission complete flag
  * @param  None
  * @retval 0 if no, 1 else
  */
uint8_t UART_CheckTransmissionComplete(void)
{
  
  if (UART_TransmissionComplete)
  {
    UART_TransmissionComplete = RESET;
    return 1;
  }
  
  return 0;
  
}

/**
  * @brief  Callback function which works after UART all received interrupt worked
  *         @note sets reception complete flag
  * @param  None
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  
  UART_ReceptionComplete = SET;
  
}

/**
  * @brief  Callback function which works after UART all transmitted interrupt worked
  *         @note sets transmission complete flag
  * @param  None
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  
  UART_TransmissionComplete = SET;
  
}

/* SPI module API ------------------------------------------------------------*/


/**
  * @brief  Checks whether all data received via SPI
  *         @note if all data received, it resets reception complete flag
  * @param  None
  * @retval 0 if no, 1 else
  */
uint8_t SPI_CheckReceptionComplete(void)
{
  
  if (SPI_ReceptionComplete)
  {
    SPI_ReceptionComplete = RESET;
    return 1;
  }
  
  return 0;
  
}

/**
  * @brief  Checks whether all data transmitted via SPI
  *         @note if all data transmitted, it resets transmission complete flag
  * @param  None
  * @retval 0 if no, 1 else
  */
uint8_t SPI_CheckTransmissionComplete(void)
{
  
  if (SPI_TransmissionComplete)
  {
    SPI_TransmissionComplete = RESET;
    return 1;
  }
  
  return 0;
  
}

/**
  * @brief  Callback function which works after SPI all received interrupt worked
  *         @note sets reception complete flag
  * @param  None
  * @retval None
  */
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *huart)
{
  
  SPI_ReceptionComplete = SET;
  
}

/**
  * @brief  Callback function which works after SPI all transmitted interrupt worked
  *         @note sets transmission complete flag
  * @param  None
  * @retval None
  */
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *huart)
{
  
  SPI_TransmissionComplete = SET;
  
}

/* Helper functions ----------------------------------------------------------*/

/**
  * @brief  Calculates size of a given array
  *         @note use if for safe transmissions, because there are many uses if strlen,
  *         and it may happen, that your buffer is 64 bytes and strlen won't work;
  *         to avoid it, use this functions
  * @param  pData is a pointer to data array
  * @retval size of array - uint8_t
  */
static uint8_t _get_size(uint8_t *pData)
{
  
  if (pData[USB_PACKET_SIZE - 1])
  {
    return USB_PACKET_SIZE;
  }
  
  return strlen((const char *)pData);
  
}

/**
  * @brief  Fills given uint8_t array with symbolic equivalent of given number
  * @param  num is number to make symbolic equivalent
  * @param  str is string whereto this symbolic equivalent must be written
  *         @note there is no checks of correct length of str; pass string that fits
  *         num size + 1 symbol for teminating 0
  * @retval None
  */
static void _itoa(uint8_t num, uint8_t *str)
{
  
  uint8_t idx = 0;
  
  do 
  {
    str[idx++] = num % 10 + '0';
    num       /= 10;
  }
  while (num);
  
  for(int lidx = 0, ridx = idx - 1; lidx != idx / 2; lidx++, ridx--)
  {
    char tmp  = str[lidx];
    str[lidx] = str[ridx];
    str[ridx] = tmp;
  }
  
  str[idx] = 0;
  
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  LED_TurnOn(ALL);
  
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************************************************* END OF FILE ****/
