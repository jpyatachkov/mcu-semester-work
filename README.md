# Курсовой проект по дисциплине МП-системы

## Задание

На основе МК линейки STM-32 создать сеть, способную осуществлять пересылку пакетов с контролем передаваемой информации при помощи CRC
по интерфейсам UART, SPI, USB, Wi-Fi. В качестве демонстрации передать пакет данных с ПЭВМ в сеть МК и обратно.

## Описание модели

Данный проект демонстрирует передачу пакета данных размером 64 байта с ПЭВМ на 2 МК STM32F072RBT6 (МК1 и МК2), установленных на отладочной плате
STM32F072-DISCOVERY.

Пакет передается в ПЭВМ по интерфейсу USB 2.0 на МК1, с него по UART на МК2, осуществляется контроль принятых данных, после чего по SPI передается
обратно на МК1 и после повторного контроля данных передается на Wi-Fi модуль ESP8266-01, работающий в режиме сервера, который ретранслирует пакет
обратно на ПЭВМ.

## Необходимая аппаратура

Для демонстрации необходимы 2 отладочные платы STM32F072-DISCOVERY, 2 кабеля USB 2.0 для их питания, Wi-Fi модуль ESP8266-01 и провода для соединений (см. ниже).

## Соединения

МК1 - использовать mini-USB user
МК2 - использовать mini-USB STLink

(формат описания соединения МК1 - МК2)

UART
PC4 - PA1
PC5 - PA0

SPI
PC2 - PC2
PC3 - PC3
PB10 - PB10

Wi-Fi на МК1
PA10, PA9

## Программная часть

Содержит две папки с проектами в STM32CubeMX и кодом в Keil MDK uVision5 - node_1 для МК1 node_2 для МК2
